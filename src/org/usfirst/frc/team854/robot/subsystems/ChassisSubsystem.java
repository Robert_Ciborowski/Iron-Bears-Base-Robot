/**
 * Name: ChassisSubsystem
 * Authors: Robert Ciborowski
 * Date: 23/08/2018
 * Description: The subsystem of our robot that represents the drive chassis.
 */

package org.usfirst.frc.team854.robot.subsystems;

import org.usfirst.frc.team854.robot.CustomSubsystem;
import org.usfirst.frc.team854.robot.RobotMode;
import org.usfirst.frc.team854.robot.recording.DigitalDeviceRecorder;
import org.usfirst.frc.team854.robot.recording.Recording;
import org.usfirst.frc.team854.robot.recording.RecordingPlayer;

public class ChassisSubsystem extends CustomSubsystem {
	private DigitalDeviceRecorder leftEncoderRecorder, rightEncoderRecorder;
	private RecordingPlayer<Double> leftRecordingPlayer, rightRecordingPlayer;

	public ChassisSubsystem() {
		currentMode = RobotMode.DISABLED;
	}

	public void init() {}

	public void periodic() {
		// We should add a switch statement here for the different modes.
		
		if (leftRecordingPlayer.isPlaying()) {
			// This gets the encoder values, which are our new target.
			Double leftValue = leftRecordingPlayer.getCurrentValue();
			Double rightValue = rightRecordingPlayer.getCurrentValue();

			// Add something which takes the encoder values and modifies the wheel motors here.
			// You can use PID for this.
		}

		if (leftEncoderRecorder.isRecording()) {
			leftEncoderRecorder.update();
			rightEncoderRecorder.update();
		}
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (enabled) {
			setCurrentMode(currentMode);
		} else {
			// Disable all PID controllers here.
		}
	}

	@Override
	public void setCurrentMode(RobotMode mode) {
		super.setCurrentMode(mode);
		if (enabled) {
			switch (mode) {
				case TELEOPERATED:
					// Disable auto-only PID controllers and reset the other PID controllers here.
					break;
				case AUTONOMOUS:
					// Call reset() on all PID controllers here.
					break;
				case DISABLED:
					// Disable all PID controllers here.
					break;
				case TEST:
					break;
				default:
					break;
			}
		}
	}

	@Override
	public void updateDashboard() {

	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		
	}

	public void startRecording() {
		// When the ports are figured out, add them to RobotInterfaceConstants and use them here:
		leftEncoderRecorder = new DigitalDeviceRecorder(/*RobotInterfaceConstants.PORT_ENCODER_LEFT*/ 0);
		rightEncoderRecorder = new DigitalDeviceRecorder(/*RobotInterfaceConstants.PORT_ENCODER_RIGHT*/ 1);
		leftEncoderRecorder.startRecording();
		rightEncoderRecorder.startRecording();
	}

	public void stopRecording() {
		leftEncoderRecorder.stopRecording();
		rightEncoderRecorder.stopRecording();
	}

	public void playRecording(Recording<Double> leftRecording, Recording<Double> rightRecording) {
		leftRecordingPlayer = new RecordingPlayer<>(leftRecording);
		leftRecordingPlayer.startPlaying();

		rightRecordingPlayer = new RecordingPlayer<>(rightRecording);
		rightRecordingPlayer.startPlaying();
	}
}
