package org.usfirst.frc.team854.robot.recording;

import java.util.ArrayList;
import java.util.List;

public class Recording<T> {
	private ArrayList<T> values = new ArrayList();
	private int timeInterval;

	public Recording(int timeInterval) {
		this.timeInterval = timeInterval;
	}

	public void addValue(T value) {
		values.add(value);
	}

	public T getValue(int time) {
		return values.get(time / timeInterval);
	}

	public long getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(int timeInterval) {
		this.timeInterval = timeInterval;
	}

	public int getNumberOfValues() {
		return values.size();
	}
}
