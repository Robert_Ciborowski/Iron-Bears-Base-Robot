package org.usfirst.frc.team854.robot.recording;

public abstract class Recorder<T> {
	protected Recording<T> recording;
	protected boolean isRecording = false;
	private long lastUpdateTime = 0;
	private long timeAtStart = 0;
	private int timeInterval;

	public Recorder(int timeInterval) {
		this.timeInterval = timeInterval;
	}

	public void update() {
		long time = System.currentTimeMillis();
		if (time >= lastUpdateTime + recording.getTimeInterval()) {
			addValueToRecording();
			lastUpdateTime = time;
		}
	}

	public abstract void addValueToRecording();

	public void startRecording() {
		recording = new Recording<T>(timeInterval);
		isRecording = true;
		timeAtStart = System.currentTimeMillis();
	}

	public void stopRecording() {
		isRecording = false;
	}

	public boolean isRecording() {
		return isRecording;
	}

	public abstract String getRecordingValuesAsString();
}
