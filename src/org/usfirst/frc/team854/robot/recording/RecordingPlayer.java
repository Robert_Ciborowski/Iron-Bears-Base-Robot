package org.usfirst.frc.team854.robot.recording;

public class RecordingPlayer<T> {
	private Recording<T> recording;
	private boolean playing = false;
	long startTime = 0;

	public RecordingPlayer(Recording<T> recording) {
		this.recording = recording;
	}

	public T getCurrentValue() {
		if (playing) {
			return recording.getValue((int) (System.currentTimeMillis() - startTime));
		} else {
			return recording.getValue(0);
		}
	}

	public void startPlaying() {
		playing = true;
		startTime = System.currentTimeMillis();
	}

	public boolean isPlaying() {
		return playing;
	}
}
