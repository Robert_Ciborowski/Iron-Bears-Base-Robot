package org.usfirst.frc.team854.robot.recording;

import org.usfirst.frc.team854.robot.Robot;
import org.usfirst.frc.team854.robot.hardware.InterfaceType;

import static org.usfirst.frc.team854.robot.constants.RecordingConstants.CHASSIS_ENCODER_RECORDING_TIME_INTERVAL;

public class DigitalDeviceRecorder extends Recorder<Double> {
	int encoderPort;

	public DigitalDeviceRecorder(int encoderPort) {
		super(CHASSIS_ENCODER_RECORDING_TIME_INTERVAL);
		this.encoderPort = encoderPort;
	}

	@Override
	public void addValueToRecording() {
		recording.addValue(Robot.devices.getDeviceValue(InterfaceType.DIGITAL, encoderPort));
	}

	@Override
	public String getRecordingValuesAsString() {
		String string = "";

		int size = recording.getNumberOfValues();
		for (int i = 0; i < size; i++) {
			string += recording.toString();

			if (i != size - 1) {
				string += ", ";
			}
		}

		return string;
	}
}
