/*
 * Class: UserInterfaceConstants
 * Authors: Everyone
 * Date: 06/01/2018
 * Description: Contains constants related to the robot's user IO interface.
 */

package org.usfirst.frc.team854.robot.constants;

import org.usfirst.frc.team854.robot.teleopdrive.TeleopDriveMode;

public interface UserInterfaceConstants {
	// These are the operator interface axes and buttons.
	public static final int PORT_JOYSTICK = 0;
	public static final int AXIS_ID_SPEED = 1;
	public static final int AXIS_ID_TURN = 0;
	public static final int AXIS_ID_INTAKE_SPEED = 3;

	public static final double JOYSTICK_TURNING_CUTOFF = 0.2;
	public static final double JOYSTICK_TURNING_MAX_RAW = 1.0;
	public static final double JOYSTICK_SPEED_CUTOFF = 0.05;
	public static final double JOYSTICK_TURNING_OFFSET = 0;
	public static final double JOYSTICK_SPEED_OFFSET = 0;

	// This defines whether the gyro PID is used during teleop.
	public static final TeleopDriveMode TELEOP_DRIVE_MODE = TeleopDriveMode.SIMPLE;
}
