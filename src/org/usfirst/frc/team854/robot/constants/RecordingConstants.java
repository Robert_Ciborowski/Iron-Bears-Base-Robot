/*
 * Class: RecordingConstants
 * Authors: Everyone
 * Date: 23/08/2018
 * Description: Contains constants related to the robot's recording feature.
 */

package org.usfirst.frc.team854.robot.constants;

public interface RecordingConstants {
	// This time interval is in milliseconds.
	int CHASSIS_ENCODER_RECORDING_TIME_INTERVAL = 16;
}
